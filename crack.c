//3
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "md5.h"
#include <sys/types.h>
#include <sys/stat.h>

const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


struct entry {
    char *hash;
    char *pass;
};


int bcompare(const void *a, const void *b){
    return strcmp(*(char **)a, *(char **)b);
}

int bs(const void *t, const void *a){
    return strcmp(t, *(char **)a);
}

int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *hash_guess = md5(guess, strlen(guess));
    // Compare the two hashes
    if (strncmp(hash, hash_guess, HASH_LEN) == 0) return 1;
    else return 0;
    // Free any malloc'd memory
    free(hash_guess);
}

int file_length(char *filename){
    struct stat fileinfo;
    if (stat(filename, &fileinfo) == -1)
        return -1;
    else
        return fileinfo.st_size;
}

struct entry *read_dictionary(char *filename, int *size){
    int len = file_length(filename);
    if (len == -1){
        printf("Couldn't get length of file %s\n", filename);
        exit(1);
    }
    
    char *file_contents = malloc(len);
    
    FILE *fp = fopen(filename, "r");
    if (!fp){
        printf("Couldn't open %s for reading\n", filename);
        exit(1);
    }
    
    fread(file_contents, 1, len, fp);
    fclose(fp);
    
    int line_count = 0;
    for (int i = 0; i < len; i++){
        if (file_contents[i] == '\n'){
            file_contents[i] = '\0';
            line_count++;
        }
    }
    
    struct entry *dict = malloc((line_count) * sizeof(struct entry));
char **line = malloc(line_count * sizeof(char *));

    int c = 0;
    for (int i = 0; i < line_count; i++){
    line[i] = &file_contents[c];
        while (file_contents[c] != '\0') c++;
        c++;
        
    *size = line_count;
    }
    


for (int i=0; i<line_count; i++){
    
    dict[i].pass = line[i];
    dict[i].hash = md5(dict[i].pass, strlen(dict[i].pass));
    
}
free(line);
return dict;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);
    
    qsort(dict, dlen, sizeof(struct entry), bcompare);
    
    FILE *h_file = fopen(argv[1], "r");
    if (!h_file){
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    struct entry *found;
    
    char hash[HASH_LEN+1];
    
    while (fgets(hash, HASH_LEN+1, h_file) != NULL){
        hash[strlen(hash)-1]='\0';
        found = bsearch(hash, dict, dlen, sizeof(struct entry), bs);
        if (found != NULL){
            printf("%s is %s!\n", found->hash, found->pass);
        }
    }
    
}